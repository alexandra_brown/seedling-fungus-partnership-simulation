`evolution_simulations.wls` runs and plots the simulation.
`evolution_times.wdx`, `phenotype_frequency_sims.wdx`, and `root_allocation_sims.wdx` are all saved simulation results that can be loaded from within `evolution_simulations.wls` instead of running the simulations, if desired.
