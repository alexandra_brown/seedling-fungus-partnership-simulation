#!/usr/bin/env wolframscript
(* ::Package:: *)

(* ::Section::Closed:: *)
(*Code*)


(* ::Subsection::Closed:: *)
(*Documentation*)


(* ::Subsubsection::Closed:: *)
(*Option: evolvingTraits*)


evolvingTraits::usage = "evolvingTraits is an option for simulateEvolution that specifies the traits that should evolve.
Possible values:
	All			        Both root allocation (\!\(\*SubscriptBox[\(r\), \(a\)]\) and \!\(\*SubscriptBox[\(r\), \(b\)]\)) and phenotype frequency (\[Phi]) evolve.
	rootAllocation		 Only root allocation evolves. Phenotype frequency remains at its initial value.
	phenotypeFrequency	 Only phenotype frequency evolves. Root allocation remains at its initial value.";


(* ::Text:: *)
(*Option values*)


rootAllocation::usage = "rootAllocation is a setting for the evolvingTraits option in the function simulateEvolution.
rootAllocation specifies that only the root allocations (\*Subscript[r, a] and \*Subscript[r, b]) should evolve.";


phenotypeFrequency::usage = "phenotypeFrequency is a setting for the evolvingTraits option in the function simulateEvolution.
phenotypeFequency specifies that only the phenotype frequency (\[Phi]) should evolve.";


(* ::Subsubsection::Closed:: *)
(*Option: initialTraits*)


initialTraits::usage = "initialTraits is an option for simulateEvolution that specifies the starting root allocation and phenotype frequency.
Possible values:
	Automatic \t  Initialize root allocations at the monophenic optimum, and phenotype frequency at 0.5.
	{{\*SubscriptBox[r,a], \*SubscriptBox[r, b]}, \[Phi]} \t Use the given values to initialize root allocations (\*SubscriptBox[r, a], \*SubscriptBox[r, b]) and phenotype frequency (\[Phi]).
	{Automatic, \[Phi]}         Use the given phenotype frequency, and initialize the root allocations at the monophenic optimum.";


(* ::Subsubsection::Closed:: *)
(*Option: mutationSize*)


mutationSize::usage = "mutationSize is an option for simulateEvolution that specifies the size of mutations.
Posible values:
	d	Mutations in root allocation and phenotype frequency are both of size d.
	{\*SubscriptBox[d, r], \*SubscriptBox[d, \[Phi]]}   Mutations in root allocation and phenotype frequency are of sizes \*SubscriptBox[s, r] and \*SubscriptBox[s, \[Phi]], respectively.";


(* ::Subsubsection::Closed:: *)
(*Option: populationSize*)


populationSize::usage = "populationSize is an option for simulateEvolution that specifies the size of the plant population.";


(* ::Subsubsection::Closed:: *)
(*Option: totalMutations*)


totalMutations::usage = "totalMutations is an option for simulateEvolution that specifies the number of successful mutant invasions to simulate.
This is essentially the number of \"time steps\" to simulate, where time is measured in mutations.";


(* ::Subsubsection::Closed:: *)
(*Error messages: wrong input*)


simulateEvolution::envPar = "There is an error in the environmental parameters:
mu1 = `1`
mu2 = `2` 
p = `3`
mu1, mu2, and p must be numbers between 0 and 1.";


simulateEvolution::evolvingTraits = "The value of the option evolvingTraits -> `1` is not All, rootAllocation, or phenotypeFrequency";


simulateEvolution::initialTraits = "The value of the option initialTraits -> `1` is not of the form {{ra, rb}, phi}, where ra, rb, and phi are numbers between 0 and 1.
The value of initialTraits may also be given as Automatic or {Automatic, phi} where phi is a number between 0 and 1.";


simulateEvolution::mutationSize = "The value of the option mutationSize -> `1` is not a positive number or a pair of positive numbers.";


simulateEvolution::omega = "`1` is not a valid value for omega. Omega must be a positive real number.";


simulateEvolution::populationsize = "The value of the option populationSize -> `1` is not a positive integer."


simulateEvolution::totalMuations = "The value of the option totalMutations -> `1` is not a positive integer.";


(* ::Subsubsection::Closed:: *)
(*Function description*)


simulateEvolution::usage = "simulateEvolution[{mu1, mu2}, p, omega] simulates the evolution of plants experiencing an environment that varies yearly between state 1 and state 2, where
	mu1 and mu2 are the optimal root allocations in states 1 and 2
	p is the frequency of state 1 years
	omega is the strength of selection
The following options can be used:
	evolvingTraits: which traits evolve. Can be specified as All, rootAllocation, or phenotypeFrequency. Default: All
	initialTraits: traits of the initial resident. Can be specified as Automatic, {Automatic, phi}, or {{ra, rb}, phi}. Default: Automatic, i.e. ra = rb = monophenic optimum, phi = 0.5
	mutationSize: size of mutations, can be specified as a single size d, or different sizes for root allocation and phetopype frequency {dr, dphi}. Default: 0.01
	populationSize: plant population size. Default: 2000
	totalMutations: total number of mutations allowed to fix before the simulate ends. Default: 150";


(* ::Subsection::Closed:: *)
(*Function code*)


(* ::Subsubsection::Closed:: *)
(*Individual plant fitness*)


(* ::Text:: *)
(*Individual plant fitness is a function of the fraction of roots allocated to fungus 1 (r) and the environmental parameters (mu, omega)*)


plantFitness[r_, mu_, omega_] := Exp[-(omega)^2 * (r - mu)^2]


(* ::Subsubsection::Closed:: *)
(*Monophenic fitness maximum*)


(* ::Text:: *)
(*Monophenic fitness is maximized at rc**)


rcStar[{mu1_, mu2_}, p_] := p * mu1 + (1 - p) * mu2


(* ::Subsubsection::Closed:: *)
(*Geometric mean fitness*)


(* ::Text:: *)
(*Log of the geometric mean fitness of polyphenism*)
(*ra and rb are the polyphenic root allocations, with phi plants allocating ra and 1 - phi allocating rb roots to fungus 1*)
(*Can be used for monophenic by setting ra = rb or phi=1 or 0 (in which case the plant will exhibit only ra or rb, respectively)*)


logPolyphenicMeanFitness[{{ra_, rb_}, phi_}, {mu1_, mu2_}, p_, omega_] := 
	p * Log[phi * plantFitness[ra, mu1, omega] + (1 - phi) * plantFitness[rb, mu1, omega]] + 
	(1 - p) * Log[phi * plantFitness[ra, mu2, omega] + (1 - phi) * plantFitness[rb, mu2, omega]]


(* ::Subsubsection::Closed:: *)
(*Probability of a mutant fixing, assuming that plants are haploid*)


rawPrFix[mutantFitness_, residentFitness_, populationSize_] :=
If[mutantFitness == residentFitness, (* catch neutral mutations before they cause errors *)
	1 / populationSize,
	(1 - Exp[-2 * (mutantFitness - residentFitness)]) / (1 - Exp[-2 * (mutantFitness - residentFitness) * populationSize])
]


(* ::Subsubsection::Closed:: *)
(*Simulate evolution*)


Options[simulateEvolution] = {
	evolvingTraits -> All, 
	initialTraits -> Automatic,
	mutationSize -> 1/100,
	populationSize -> 2000,
	totalMutations -> 150};


simulateEvolution[{mu1_, mu2_}, p_, omega_, opts:OptionsPattern[]] :=

Module[{resident, residentFitness, possibleMutations, mutants, mutantFitnesses, mutantFixationProbs, 

	evolutionaryHistory = Table[{{"NA", "NA"}, "NA"}, OptionValue[totalMutations] + 1], 
	totalFixationProbHistory = Table["NA", OptionValue[totalMutations] + 1],
	
	totalMutations = OptionValue[totalMutations],
	populationSize = OptionValue[populationSize]},
	
	(* Make a list of possible mutations *)
	Module[{dr, dphi}, 
	
		(* Mutation sizes for root allocation (dr) and phenotype frequency (dphi) *)
		{dr, dphi} = Switch[OptionValue[mutationSize],
			_, (* A single number was given for mutation size *)
				{OptionValue[mutationSize], OptionValue[mutationSize]},
			{_, _}, (* Two numbers were given for mutation size *)
				OptionValue[mutationSize]];
		
		possibleMutations = Switch[OptionValue[evolvingTraits],
			All,
				{{{-dr, 0}, 0}, {{dr, 0}, 0}, {{0, -dr}, 0}, {{0, dr}, 0}, {{0, 0}, -dphi}, {{0, 0}, dphi}},
			rootAllocation,
				{{{-dr, 0}, 0}, {{dr, 0}, 0}, {{0, -dr}, 0}, {{0, dr}, 0}},
			phenotypeFrequency,
				{{{0, 0}, -dphi}, {{0, 0}, dphi}}];
		];
	
	
	(* Initialize the resident's traits, {{ra, rb}, phi} *)
	resident = Switch[OptionValue[initialTraits],
		Automatic, (* Initialize root allocation at the monophenic optimum, rcStar. Initialize phenotype frequency at 0.5 *)
			{{rcStar[{mu1, mu2}, p], rcStar[{mu1, mu2}, p]}, 0.5},
		{Automatic, _}, (* Initialize root allocation at the monophenic optimum. Initialize phenotype frequency at the given value *)
			{{rcStar[{mu1, mu2}, p], rcStar[{mu1, mu2}, p]}, OptionValue[initialTraits][[2]]},
		{{_, _}, _}, (* Initialize all traits using the given values *)
			OptionValue[initialTraits]];
	
	residentFitness = Exp[logPolyphenicMeanFitness[resident, {mu1, mu2}, p, omega]];
	evolutionaryHistory[[1]] = resident;
	
	(* Simulate mutant invasions *)
	Do[
		(* Generate mutants and calculate their fixation probabilities *)
		mutants = Map[resident + #&, possibleMutations];
		
		(*discard mutants with ra, rb, or phi not between 0 and 1 *)
		mutants = Select[mutants, AllTrue[Flatten[#], Between[{0, 1}]]&];
		
		(* Calculate mutant invasion fitness and fixation probability *)
		mutantFitnesses = Map[Exp[logPolyphenicMeanFitness[#, {mu1, mu2}, p, omega]]&, mutants];
		mutantFixationProbs = Map[N[rawPrFix[#, residentFitness, populationSize]]&, mutantFitnesses];
		
		(*Print[mutantFixationProbs];*)
		
		(* Randomly choose a mutant to fix in the population (becoming the new resident), based on fixation probabilities *)
		resident = RandomSample[mutantFixationProbs/Total[mutantFixationProbs] -> mutants, 1][[1]];
		residentFitness = Exp[logPolyphenicMeanFitness[resident, {mu1, mu2}, p, omega]];
		
		evolutionaryHistory[[t]] = resident;
		totalFixationProbHistory[[t]] = Total[mutantFixationProbs],
	
	{t, 2, totalMutations + 1}];
	
	Return[{evolutionaryHistory, totalFixationProbHistory}]
	
] /; (* Here we check the inputs. The "/;" tells Mathematica the function is only defined if what follows it returns True *)
	checkInput[{mu1, mu2}, p, omega, opts]


(* ::Subsubsection::Closed:: *)
(*Check input to simulation*)


(* ::Text:: *)
(*Function to check the input to simulateEvolution*)


Options[checkInput] = Options[simulateEvolution]


checkInput[{mu1_, mu2_}, p_, omega_, opts:OptionsPattern[]] :=
	And[
		(* mu1, mu2, and p (the single-environment optima and frequency of environment 1) must be real numbers between 0 and 1*)
		If[AllTrue[{mu1, mu2, p}, NumericQ[#] && (Im[#] == 0) && Between[#, {0, 1}]&],
			True,
			Message[simulateEvolution::envPar, mu1, mu2, p]],
	
		(* omega, the strength of selection, must be a positive number *)
		If[NumericQ[omega] && (Im[omega] == 0) && Positive[omega],
			True,
			Message[simulateEvolution::omega, omega]],
	
		(* evolvingTraits option must be one of All, rootAllocation, or phenotypeFrequency *)
		If[MemberQ[{All, rootAllocation, phenotypeFrequency}, OptionValue[evolvingTraits]], 
			True,
			Message[simulateEvolution::evolvingTraits, OptionValue[evolvingTraits]]],
	
		(* initialTraits option must be either Automatic, {Automatic, phi}, or {{ra, rb}, phi} *)
		If[MatchQ[OptionValue[initialTraits], 
				Automatic | {Automatic, phi_?NumberQ/;Between[phi, {0, 1}]} | {{ra_?NumberQ, rb_?NumberQ}, phi_?NumberQ}/;AllTrue[{ra, rb, phi}, Between[{0, 1}]]],
			True,
			Message[simulateEvolution::initialTraits, OptionValue[initialTraits]]],
		
		(* mutationSize option must be either one or two positive numbers *)
		If[MatchQ[OptionValue[mutationSize], _?Positive | {_?Positive, _?Positive}],
			True,
			Message[simulateEvolution::mutationSize, OptionValue[mutationSize]]],
		
		(* populationSize option must be a positive integer *)
		If[(IntegerQ[OptionValue[populationSize]] && Positive[OptionValue[populationSize]]),
			True,
			Message[simulateEvolution::populationSize, OptionValue[populationSize]]],
	
		(* totalMutations option must be a positive integer *)
		If[(IntegerQ[OptionValue[totalMutations]] && Positive[OptionValue[totalMutations]]),
			True,
			Message[simulateEvolution::totalMutations, OptionValue[totalMutations]]]
	]


(* ::Subsubsection::Closed:: *)
(*Simulate mutation fixation time*)


(* ::Text:: *)
(*Mutation rate here is the per-generation mutation rate*)


simulateFixationTime[totalFixationProb_, mutationRate_:1/100] :=
	1 + RandomVariate[GeometricDistribution[totalFixationProb * mutationRate]]


(* ::Subsection::Closed:: *)
(*Formatting code*)


(* ::Subsubsection::Closed:: *)
(*Strings with subscripts and superscripts*)


sub[x_, y_] := ToString[Subscript[x, y], StandardForm]


sup[x_, y_] := ToString[Superscript[x, y], StandardForm]


(* ::Subsubsection::Closed:: *)
(*Color function*)


myColorFunction[min_, max_] :=
	If[0 <= min <= 1 <= max,
		(* For nonnegative data ranges that include 1 (i.e. fitness ratios), "center" the color scheme around 1 *)
		ColorData["BrownCyanTones"][(# - 1) / (2 * Max[1 - min, max - 1]) + 0.5]&,
		(* For other data ranges, center the color scheme around 0 (these ranges will likely be log fitness values) *)
		ColorData["BrownCyanTones"][ # / (2 * Max[Abs[min], Abs[max]] ) + 0.5]&
	]


(* ::Subsubsection::Closed:: *)
(*rc* marker*)


(* ::Text:: *)
(*A star to mark rc* on plots*)


star = Graphics[WindingPolygon[CirclePoints[5][[{1, 3, 5, 2, 4}]]]];


(* ::Subsubsection::Closed:: *)
(*Label style*)


myLabelStyle = {FontColor -> Black, FontFamily -> "Arial", FontSize -> 12};


(* ::Section:: *)
(*Run simulations*)


(* ::Subsection::Closed:: *)
(*Only root allocation evolves (Figs 4 and S1)*)


(* ::Text:: *)
(*Run the simulations (or import already-saved simulations)*)


If[FileExistsQ[NotebookDirectory[] <> "root_allocation_sims.wdx"] && ChoiceDialog["Saved root allocation simulations exist. Would you like to import them?"],

	(* If saved simulation data exists and the user says yes, import it *)
	simsRA = Import[NotebookDirectory[] <> "root_allocation_sims.wdx"],
	
	(* Otherwise, run the simulations *)
	simsRA = ParallelTable[simulateEvolution[{1/4 (* mu1*), 3/4 (* mu2 *)}, 2/3 (* p *), 4 (* omega *), 
			evolvingTraits -> rootAllocation,
			initialTraits -> {Automatic, phiInit},
			mutationSize -> 1/500,
			populationSize -> 10000,
			totalMutations -> 1000], 
		{phiInit, 0, 5/10, 1/20}, 100];
];


(* ::Text:: *)
(*Save the simulations*)


Export[NotebookDirectory[] <> "root_allocation_sims.wdx", N[simsRA]];


(* ::Subsubsection::Closed:: *)
(*Figure S1: Endpoints of simulations and fitness surfaces*)


(* ::Text:: *)
(*Panel a: endpoints of simulations*)


allSimEndpoints = ListPlot[simsRA[[All, All, 1, -1, 1]], 
	AspectRatio -> 1,
	Frame -> {True, True, False, False},
	FrameLabel -> {sub["r", "a"], sub["r", "b"]},
	FrameStyle -> Black,
	ImageSize -> 72 * 3.5,
	LabelStyle -> myLabelStyle,
	PlotLegends -> BarLegend[{"BrownCyanTones", {0, 0.5}}, Table[phi, {phi, 0, 0.5, 0.05}],
		LabelStyle -> myLabelStyle,
		LegendLabel -> "\[Phi]",
		LegendMarkerSize -> 72 * 3],
	PlotRange -> {{0, 1}, {0, 1}}, 
	PlotStyle -> "BrownCyanTones"];

allSimEndpoints = Labeled[allSimEndpoints, 
	Pane[Style["(a)", FontColor -> Black, FontFamily -> "Arial", FontSize -> 12],  72 * 4.3], Top]


(* ::Text:: *)
(*Panels b-d: Fitness surfaces at different phenotype frequencies*)


exampleFitnessSurfaces = Table[
ContourPlot[
	Exp[logPolyphenicMeanFitness[{{ra, rb}, phi}, {1/4, 3/4}, 2/3, 4] -
		logPolyphenicMeanFitness[{{rcStar[{1/4, 3/4}, 2/3], rcStar[{1/4, 3/4}, 2/3]}, 0.05}, {1/4, 3/4}, 2/3, 4]], 
	{ra, 0, 1}, {rb, 0, 1},
	ColorFunction -> myColorFunction[0, 1.4],
	ColorFunctionScaling -> False,
	Contours -> Range[0, 1.3, 0.1],
	ContourStyle -> Map[If[# == 1, Thick, Automatic]&, Range[0, 1.3, 0.1]],
	Epilog -> Inset[star, {rcStar[{1/4, 3/4}, 2/3], rcStar[{1/4, 3/4}, 2/3]}, Center, Scaled[0.08]],
	FrameLabel -> {sub["r", "a"], sub["r", "b"]},
	FrameStyle -> myLabelStyle,
	ImageSize -> 72 * 2,
	LabelStyle -> myLabelStyle,
	PlotRange -> {{0, 1}, {0, 1}}],
{phi, {0.1, 0.3, 0.5}}];


(* ::Text:: *)
(*Combine panels into figure*)


Grid[{{allSimEndpoints, SpanFromLeft, SpanFromLeft},

	{Labeled[Show[exampleFitnessSurfaces[[1]], ListPlot[simsRA[[3, All, 1, -1, 1]], PlotStyle -> Black]], 
		Pane[Style[("(b)\t    \[Phi] = 0.1"), FontColor -> Black, FontFamily -> "Arial", FontSize -> 12],  72 * 2], Top],

	Labeled[Show[exampleFitnessSurfaces[[2]], ListPlot[simsRA[[7, All, 1, -1, 1]], PlotStyle -> Black]],
		Pane[Style[("(c)\t    \[Phi] = 0.3"), FontColor -> Black, FontFamily -> "Arial", FontSize -> 12],  72 * 2], Top],
		
	Labeled[Show[exampleFitnessSurfaces[[3]], ListPlot[simsRA[[11, All, 1, -1, 1]], PlotStyle -> Black]],
		Pane[Style[("(d)\t    \[Phi] = 0.5"), FontColor -> Black, FontFamily -> "Arial", FontSize -> 12],  72 * 2], Top]},
	
	{BarLegend[{myColorFunction[0, 1.4], {0, 1.4}}, Range[0, 1.3, 0.1],
		LabelStyle -> myLabelStyle,
		LegendLabel -> "Fitness",
		LegendLayout -> "Row",
		LegendMarkerSize -> 72 * 3],
	SpanFromLeft, SpanFromLeft}}]
	
Export[NotebookDirectory[] <> "root_allocation_sim_all_outcomes.pdf", %]


(* ::Subsubsection::Closed:: *)
(*Figure 4: Optimal (evolved) root allocation*)


(* ::Text:: *)
(*Separate simulations based on their endpoints*)


highPeakPts = Map[Select[#[[1, 1]] > #[[1, 2]]&], simsRA[[All, All, 1, -1]]]; (* Simulations that ended with ra > rb *)
lowPeakPts = Map[Select[#[[1, 1]] <= #[[1, 2]]&], simsRA[[All, All, 1, -1]]]; (* Simulations that ended with rb >= ra *)


(* ::Text:: *)
(*Fraction of simulations with ra > rb (it's about half, no pattern based on phenotype frequency)*)


Map[Length, highPeakPts] / (Map[Length, highPeakPts] + Map[Length, lowPeakPts]) //N
ListLinePlot[% * 100, 
	AxesLabel -> {"\[Phi]"},
	DataRange -> {0, 0.5},
	PlotLabel -> "% of simulations ending with " <> sub["r", "a"] <> " > " <> sub["r", "b"]]


(* ::Subsubsubsection::Closed:: *)
(*Plot mean and variance in simulation endpoints*)


(* ::Text:: *)
(*Function to plot mean and standard deviation of simulation endpoints*)


plotPeakPts[pts_] :=
ListLinePlot[Transpose[
	MapThread[{
		{#1, Mean[#2][[1]] - StandardDeviation[#2][[1]]},
		{#1, Mean[#2][[1]] + StandardDeviation[#2][[1]]}, 
		{#1, Mean[#2][[2]] - StandardDeviation[#2][[2]]},
		{#1, Mean[#2][[2]] + StandardDeviation[#2][[2]]},
		{#1, Mean[#2][[1]]}, 
		{#1, Mean[#2][[2]]}}&, 
		{Table[phi, {phi, 0, 5/10, 1/20}], pts}]],
	AspectRatio -> 0.8,
	Epilog -> {
		Text[Style[sub["\[Mu]", "1"], FontSize -> 12], {0.35, 0.25}, {0, 1}], 
		Text[Style[sub["\[Mu]", "2"], FontSize -> 12], {0.35, 0.75}, {0, -1}], 
		Text[Style[sup[sub["r", "c"], "*"], FontSize -> 12], {0.35, 5/12}, {0, -1}]},
	Filling -> {1 -> {{2}, ColorData["BrownCyanTones"][0.3]}, {3 -> {{4}, ColorData["BrownCyanTones"][0.7]}}},
	Frame -> {True, True, False, False},
	FrameLabel -> {"Trait expression (\[Phi])", "Root allocation"},
	FrameStyle -> Black,
	ImageSize -> 72 * 3,
	LabelStyle -> {FontColor -> Black, FontFamily -> "Arial", FontSize -> 12},
	PlotRange -> {{0, 0.5}, {0, 1}},
	PlotStyle -> {
		ColorData["BrownCyanTones"][0.3], 
		ColorData["BrownCyanTones"][0.3], 
		ColorData["BrownCyanTones"][0.7], 
		ColorData["BrownCyanTones"][0.7],
		ColorData["BrownCyanTones"][0.1], 
		ColorData["BrownCyanTones"][0.95]},
	Prolog -> {Gray, Line[{{0, 0.75}, {0.5, 0.75}}], Line[{{0, 0.25}, {0.5, 0.25}}], Line[{{0, 5/12}, {1, 5/12}}]}]


(* ::Text:: *)
(*Plot the mean and standard deviation of simulation endpoints, sorted by whether they ended up with ra > rb or ra < rb*)


lowPeakPlot = Show[plotPeakPts[lowPeakPts[[All, All, 1]]], 
	FrameLabel -> {"Trait expression (\[Phi])", ""}];
	
lowPeakPlot = Legended[lowPeakPlot,
				Placed[LineLegend[
					{ColorData["BrownCyanTones"][0.1], ColorData["BrownCyanTones"][0.95]}, 
					{sub["r", "a"], sub["r", "b"]}, 
					LabelStyle -> {FontColor -> Black, FontFamily -> "Arial", FontSize -> 12},
					LegendLayout -> "Row",
					LegendMarkerSize -> 25], {0.5, 0.975}]]

highPeakPlot = plotPeakPts[highPeakPts[[All, All, 1]]];


(* ::Subsubsubsection::Closed:: *)
(*Plot the highest fitness point any simulation ended up at*)


(* ::Text:: *)
(*Find the fitness each simulation ended up at (where simulation ended up with ra > rb)*)


highPeakFitnesses = Map[Exp[logPolyphenicMeanFitness[#, {1/4 (* mu1*), 3/4 (* mu2 *)}, 2/3 (* p *), 4 (* omega *)] - 
		logPolyphenicMeanFitness[{{rcStar[{1/4, 3/4}, 2/3], rcStar[{1/4, 3/4}, 2/3]}, 1}, {1/4 (* mu1*), 3/4 (* mu2 *)}, 2/3 (* p *), 4 (* omega *)]]&,
	highPeakPts, {2}];


(* ::Text:: *)
(*Find the fitness each simulation ended up at (where simulation ended up with ra <= rb)*)


lowPeakFitnesses = Map[Exp[logPolyphenicMeanFitness[#, {1/4 (* mu1*), 3/4 (* mu2 *)}, 2/3 (* p *), 4 (* omega *)] - 
		logPolyphenicMeanFitness[{{rcStar[{1/4, 3/4}, 2/3], rcStar[{1/4, 3/4}, 2/3]}, 1}, {1/4 (* mu1*), 3/4 (* mu2 *)}, 2/3 (* p *), 4 (* omega *)]]&,
	lowPeakPts, {2}];


(* ::Text:: *)
(*Plot the maximum fitness any simulation ended up at vs phenotype frequency*)


peakHeightsPlot = ListLinePlot[{
	MapThread[{#1, Max[#2]}&, {Table[phi, {phi, 0, 1/2, 1/20}], N[highPeakFitnesses]}],
	MapThread[{#1, Max[#2]}&, {Table[phi, {phi, 0, 1/2, 1/20}], N[lowPeakFitnesses]}]},
	Epilog -> {Text[Style["Higher peak", FontSize -> 12], {0.125, 1.3}, {0, 1}],
		Text[Style["Lower peak", FontSize -> 12], {0.45, 1.1}, {0, 1}]},
	Frame -> {True, True, False, False},
	FrameLabel -> {"Phenotype frequency (\[Phi])", "Fitness"},
	FrameStyle -> Black,
	ImageSize -> 72 * 4.5,
	LabelStyle -> {FontColor -> Black, FontFamily -> "Arial", FontSize -> 12},
	PlotStyle -> {
		ColorData["BrownCyanTones"][0], 
		{Dashed, ColorData["BrownCyanTones"][0]}}]


(* ::Subsubsubsection::Closed:: *)
(*Assemble plots into figure*)


Grid[{
	(* Row 1: heights of fitness peaks *)
	{Labeled[peakHeightsPlot, 
		Pane[Style["(a)", FontColor -> Black, FontFamily -> "Arial", FontSize -> 12], 72 * 4.5], Top], 
	SpanFromLeft},
	
	(*Row 2: locations of fitness peaks *)
	{Labeled[highPeakPlot,
		Pane[Style["(b)\t\tHigher peak", FontColor -> Black, FontFamily -> "Arial", FontSize -> 12], 72 * 3], Top],

	Labeled[lowPeakPlot,
		Pane[Style["(c)\t\tLower peak", FontColor -> Black, FontFamily -> "Arial", FontSize -> 12], 72 * 3], Top]}},

	(* Grid spacing*)
	Spacings -> {1, {Automatic, 2}}	
]

Export[NotebookDirectory[] <> "root_allocation_sims_fitness_peaks.pdf", %]


(* ::Subsection::Closed:: *)
(*Evolving root allocation & phenotype frequency vs environmental frequency (Fig 6)*)


If[FileExistsQ[NotebookDirectory[] <> "phenotype_frequency_sims.wdx"] && ChoiceDialog["Saved phenotype frequency simulations exist. Would you like to import them?"],

	(* If saved simulation data exists and the user says yes, import it *)
	phiSimsEndpoints = Import[NotebookDirectory[] <> "phenotype_frequency_sims.wdx"],
	
	(* Otherwise, run the simulations *)
	phiSims = ParallelTable[simulateEvolution[{1/4 (* mu1*), 3/4 (* mu2 *)}, p (* p *), 4 (* omega *), 
			evolvingTraits -> All,
			initialTraits -> Automatic,
			mutationSize -> 1/500,
			populationSize -> 10000,
			totalMutations -> 1000], 
		{p, 2/10, 5/10, 1/40}, 100];
	
	phiSimsEndpoints = Map[Append@@#&, phiSims[[All, All, 1, -1]], {2}];
		
	(* Export the simulations *)
	Export[NotebookDirectory[] <> "phenotype_frequency_sims.wdx", phiSimsEndpoints]
];


(* ::Text:: *)
(*Check that polyphenism can invade at each p value*)


Solve[4^2 == 1 / (2 * (3/4 - 1/4)^2 * (1 - p) * p), p] //N


(* ::Text:: *)
(*Plot the phenotype frequency of the highest-fitness endpoint (where ra > rb)*)


MapThread[Map[Function[x, {x, logPolyphenicMeanFitness[{{x[[1]], x[[2]]}, x[[3]]}, {1/4, 3/4}, #2, 4]}], #1]&, 
	{Map[Select[#[[1]] > #[[2]]&], phiSimsEndpoints], Range[2/10, 5/10, 1/40]}];
Map[SortBy[#[[2]]&], %];
%[[All, -1, 1, 3]];
MapThread[{#2, #1}&,  {%, Range[2/10, 5/10, 1/40]}];
phiPlotb = ListLinePlot[%, 
	AspectRatio -> 1,
	Frame -> {True, True, False, False},
	FrameLabel -> {"Frequency of Env. 1, p", "Phenotype frequency (\[Phi])"},
	ImageSize -> 72 * 4,
	LabelStyle -> myLabelStyle,
	PlotStyle -> ColorData["BrownCyanTones"][0.1],
	PlotMarkers -> {Automatic, Small},
	Prolog -> {Black, Dashed, Line[{{0, 1}, {1, 0}}]}]


(* ::Text:: *)
(*Plot the phenotype frequency of the highest-fitness endpoint (where ra < rb)*)


MapThread[Map[Function[x, {x, logPolyphenicMeanFitness[{{x[[1]], x[[2]]}, x[[3]]}, {1/4, 3/4}, #2, 4]}], #1]&, 
	{Map[Select[#[[1]] < #[[2]]&], phiSimsEndpoints], Range[2/10, 5/10, 1/40]}];
Map[SortBy[#[[2]]&], %];
%[[All, -1, 1, 3]];
MapThread[{#2, #1}&,  {%, Range[2/10, 5/10, 1/40]}];
phiPlota = ListLinePlot[%, 
	AspectRatio -> 1,
	Frame -> {True, True, False, False},
	FrameLabel -> {"Frequency of Env. 1, p", "Phenotype frequency (\[Phi])"},
	ImageSize -> 72 * 4,
	LabelStyle -> myLabelStyle,
	PlotStyle -> ColorData["BrownCyanTones"][1],
	PlotMarkers -> {Automatic, Small},
	Prolog -> {Black, Dashed, Line[{{0.1, .1}, {0.6, 0.6}}]}]


MapThread[
	Labeled[#1, 
		Pane[Style[#2, myLabelStyle], 72 * 4], 
		Top]&,
	{{phiPlota, phiPlotb}, {"(a)", "(b)"}}];
	
Row[%, Spacer[20]]

Export[NotebookDirectory[] <> "optimal_phi.pdf" , %];


(* ::Text:: *)
(*The differences between the frequency of the rarer phenotype and the rarer environmental state*)


MapThread[Map[Function[x, {x, logPolyphenicMeanFitness[{{x[[1]], x[[2]]}, x[[3]]}, {1/4, 3/4}, #2, 4]}], #1]&, 
	{Map[Select[#[[1]] < #[[2]]&], phiSimsEndpoints], Range[2/10, 5/10, 1/40]}];
Map[SortBy[#[[2]]&], %];
%[[All, -1, 1, 3]];
raEnv1Pheno = MapThread[{#2, #2 - #1}&,  {%, Range[2/10, 5/10, 1/40]}];
raEnv1Fitnesses = %%%[[All, -1, 2]]

MapThread[Map[Function[x, {x, logPolyphenicMeanFitness[{{x[[1]], x[[2]]}, x[[3]]}, {1/4, 3/4}, #2, 4]}], #1]&, 
	{Map[Select[#[[1]] > #[[2]]&], phiSimsEndpoints], Range[2/10, 5/10, 1/40]}];
Map[SortBy[#[[2]]&], %];
%[[All, -1, 1, 3]]
rbEnv1Pheno = MapThread[{#2, #2 - (1 - #1)}&,  {%, Range[2/10, 5/10, 1/40]}];
rbEnv1Fitnesses = %%%[[All, -1, 2]]



ListPlot[{raEnv1Pheno, rbEnv1Pheno}, 
	AspectRatio -> 1,
	Epilog -> Line[{{0.2, .2}, {0.5, 0.5}}],
	Frame -> {True, True, False, False},
	FrameLabel -> {"Frequency of env. 1, p", "Frequency of env. 1 - frequency of env. 1 phenotype"},
	ImageSize -> 72 * 4,
	LabelStyle -> myLabelStyle,
	PlotStyle -> {ColorData["BrownCyanTones"][1], ColorData["BrownCyanTones"][0.05]},
	PlotMarkers -> {Automatic, Medium},
	PlotRange -> Full]


(* ::Text:: *)
(*The evolved root allocation traits of the most-fit strategies*)


MapThread[Map[Function[x, {x, logPolyphenicMeanFitness[{{x[[1]], x[[2]]}, x[[3]]}, {1/4, 3/4}, #2, 4]}], #1]&, 
	{Map[Select[#[[1]] > #[[2]]&], phiSimsEndpoints], Range[2/10, 5/10, 1/40]}];
Map[SortBy[#[[2]]&], %];
raGreaterPts = %[[All, -1, 1, 1;;2]];

MapThread[Map[Function[x, {x, logPolyphenicMeanFitness[{{x[[1]], x[[2]]}, x[[3]]}, {1/4, 3/4}, #2, 4]}], #1]&, 
	{Map[Select[#[[1]] < #[[2]]&], phiSimsEndpoints], Range[2/10, 5/10, 1/40]}];
Map[SortBy[#[[2]]&], %];
rbGreaterPts = %[[All, -1, 1, 1;;2]];


ListPlot[{raPts, rbPts}]


(* ::Text:: *)
(*A quick check that we don't any have simulations where ra = rb*)


Map[Select[#[[1]] == #[[2]]&], phiSimsEndpoints]


(* ::Subsubsection::Closed:: *)
(*Fitness comparisons to check that the evolved traits are more fit than matching the environment exactly*)


(* ::Text:: *)
(*Positive numbers here indicate the evolved traits are more fit than the comparison traits*)


(* ::Text:: *)
(*Compare the evolved max. fitness to being at the single environment optima (at the environmental frequency)*)


MapThread[Map[Function[x, {x, logPolyphenicMeanFitness[{{x[[1]], x[[2]]}, x[[3]]}, {1/4, 3/4}, #2, 4]}], #1]&, 
	{phiSimsEndpoints[[All, 1;;100]], Range[2/10, 5/10, 1/40]}];
Map[SortBy[#[[2]]&], %];
%[[All, -1]];
Table[logPolyphenicMeanFitness[{{0.25, 0.75}, p}, {0.25, 0.75}, p, 4], {p, 2/10, 5/10, 1/40}];
MapThread[#1[[2]] - #2&, {%%, %}]


(* ::Text:: *)
(*Compare the evolved max. fitness to expressing the root allocation traits at the same frequency as the environment*)


MapThread[Map[Function[x, {x, logPolyphenicMeanFitness[{{x[[1]], x[[2]]}, x[[3]]}, {1/4, 3/4}, #2, 4]}], #1]&, 
	{Map[Select[#[[1]] < #[[2]]&], phiSimsEndpoints], Range[2/10, 5/10, 1/40]}];
Map[SortBy[#[[2]]&], %];
%[[All, -1]];
MapThread[logPolyphenicMeanFitness[{{#1[[1]], #1[[2]]}, #2}, {0.25, 0.75}, #3, 4]&, {%[[All, 1]], Range[2/10, 5/10, 1/40], Range[2/10, 5/10, 1/40]}];
MapThread[#1[[2]] - #2&, {%%, %}]


(* ::Text:: *)
(*Compare the evolved max. fitness to expressing the single environment optima at the evolved phenotype frequency*)


MapThread[Map[Function[x, {x, logPolyphenicMeanFitness[{{x[[1]], x[[2]]}, x[[3]]}, {1/4, 3/4}, #2, 4]}], #1]&, 
	{Map[Select[#[[1]] < #[[2]]&], phiSimsEndpoints], Range[2/10, 5/10, 1/40]}];
Map[SortBy[#[[2]]&], %];
%[[All, -1]];
MapThread[logPolyphenicMeanFitness[{{0.25, 0.75}, #1[[3]]}, {0.25, 0.75}, #3, 4]&, {%[[All, 1]], Range[2/10, 5/10, 1/40], Range[2/10, 5/10, 1/40]}];
MapThread[#1[[2]] - #2&, {%%, %}]


(* ::Subsection::Closed:: *)
(*Comparing evolution time (Fig 7)*)


If[FileExistsQ[NotebookDirectory[] <> "evolution_times.wdx"] && ChoiceDialog["Saved evolution time simulations exist. Would you like to import them?"],
	
	(* If saved simulation data exists and the user says yes, import it *)
	earlyTimesP = Import[NotebookDirectory[] <> "evolution_times.wdx"];
	
	(* Also record that we won't need to re-run the simulations *)
	runTimeSims = False;,
	
	(* Otherwise, record that we will re-run the simulations *)
	runTimeSims = True];


(* ::Text:: *)
(*Simulate evolution, if saved simulations weren't loaded. Note that this and the fixation time simulation below take ~40 min total to run in parallel. However, the resulting outputs are very difficult to save and load (over an hour to save, and we on several occasions crashed Mathematica trying to load them), so we save and load summary information instead.*)


If[runTimeSims,
	(* Run the simulations *)
	simsP = Table[
			ParallelTable[simulateEvolution[{1/4 (* mu1*), 3/4 (* mu2 *)}, p, 4 (* omega *), 
				evolvingTraits -> rootAllocation,
				initialTraits -> Automatic,
				populationSize -> 10000,
				mutationSize -> m,
				totalMutations -> 1000],
				1000],
			{m, {1/200, 1/100}}, {p, 0.2, 0.5, 0.01}]
];


(* ::Text:: *)
(*Simulate the time each mutation took to fix*)


If[runTimeSims,
	timesP = ParallelMap[simulateFixationTime[#, 10000 * 1/1000]&, simsP[[All, All, All, 2, 2;;All]], {4}]
];


(* ::Text:: *)
(*Find the time to exit the region near the monophenic optimum*)


If[runTimeSims,
	regionExitEvent = Table[Module[{p = Table[q, {q, 0.2, 0.5, 0.01}][[i]]},
		Map[FirstPosition[{{a_, b_}, h_}/;(Norm[{a, b} - {rcStar[{1/4, 3/4}, p], rcStar[{1/4, 3/4}, p]}] >= 0.01)], simsP[[m, i, All, 1]]]],
		{m, 2}, {i, 31}];
	earlyTimesP = MapThread[Total[#1[[1;;(#2[[1]] + 1)]]]&, {timesP, regionExitEvent}, 3];
];


(* ::Text:: *)
(*Report the mean waiting times*)


Map[Mean, earlyTimesP, {2}] //N
ListLinePlot[%, DataRange -> {0.2, 0.5}, PlotLegends -> {"mutation size 1/200", "mutation size 1/100"}, 
FrameLabel -> {"p", "Time to exit region near\nmonophenic optimum (generations)"}, Frame -> {True, True, False, False},
LabelStyle -> {FontSize -> 12, FontColor -> Black, FontFamily -> "Arial"},
FrameStyle -> Black]
Map[StandardDeviation, %%%, {2}] //N


(* ::Text:: *)
(*Save simulated exit times*)


If[runTimeSims,
	Export[NotebookDirectory[] <> "evolution_times.wdx", N[earlyTimesP]]
];


Map[Mean, earlyTimesP, {2}];
ListLinePlot[%, DataRange -> {0.2, 0.5}, PlotLegends -> {(*"mutation size 1/500", *)"mutation size 1/200", "mutation size 1/100"}, 
FrameLabel -> {"Frequency of Env. 1 (p)", "Time to exit region near\nmonophenic optimum (generations)"}, Frame -> {True, True, False, False},
LabelStyle -> {FontSize -> 12, FontColor -> Black, FontFamily -> "Arial"},
FrameStyle -> Black,
PlotStyle -> {Directive[ColorData["BrownCyanTones"][0], Dashed], ColorData["BrownCyanTones"][1]}]
Export["early_evolution_time.pdf", %]






